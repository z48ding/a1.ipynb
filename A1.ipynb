{
  "nbformat": 4,
  "nbformat_minor": 0,
  "metadata": {
    "kernelspec": {
      "display_name": "Python 3",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.6.8"
    },
    "colab": {
      "provenance": []
    }
  },
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "YLRqn3y2NfJi"
      },
      "source": [
        "## CS431/631 Big Data Infrastructure\n",
        "### Winter 2023 - Assignment 1\n",
        "---"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "K2dtUlLTNfJl"
      },
      "source": [
        "\n",
        "* **Name:** Zetong Ding\n",
        "* **ID:** 20783852"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "z3DCzT-rNfJm"
      },
      "source": [
        "---\n",
        "#### Overview\n",
        "For this assignment, you will be using Python to analyze the [pointwise mutual information (PMI)](http://en.wikipedia.org/wiki/Pointwise_mutual_information) of tokens in the text of Shakespeare's plays.    For this assignment, you will need the same text file (`Shakespeare.txt`) that you used for Assignment 0.   You will also need the Python tokenizer module, `simple_tokenize.py`.\n",
        "\n",
        "If two events $x$ and $y$ are independent, their PMI will be zero.   A positive PMI indicates that $x$ and $y$ are more likely to co-occur than they would be if they were independent.   Similarly, a negative PMI indicates that $x$ and $y$ are less likely to co-occur.   The PMI of events $x$ and $y$ is given by\n",
        "\\begin{equation*}\n",
        "PMI(x,y) = \\log\\frac{p(x,y)}{p(x)p(y)}\n",
        "\\end{equation*}\n",
        "where $p(x)$ and $p(y)$ are the probabilities of occurrence of events $x$ and $y$, and $p(x,y)$ is the probability of co-occurrence of $x$ and $y$.\n",
        "\n",
        "For this assignment, the \"events\" that we are interested in are occurrences of tokens on lines of text in the input file.   For example, one event\n",
        "might represent the occurence of the token \"fire\" a line of text, and another might represent the occurrence of the token \"peace\".   In that case, $p(fire)$ represents the probability that \"fire\" will occur on a line of text, and $p(fire,peace)$ represents the probability that *both* \"fire\" and \"peace\" will occur on the *same* line.   For the purposes of these PMI computations, it does not matter how many times a given token occures on a single line.   Either a line contains a particular token (at least once), or it does not.   For example, consider this line of text:\n",
        "\n",
        "> three three three, said thrice\n",
        "\n",
        "For this line, the following token-pair events have occurred:\n",
        "- (three, said)\n",
        "- (three, thrice)\n",
        "- (said, three)\n",
        "- (said, thrice)\n",
        "- (thrice, three)\n",
        "- (thrice, said)\n",
        "\n",
        "Note that we are not interested in \"reflexive\" pairs, such as (thrice,thrice).\n",
        "\n",
        "In addition to the probabilities of events, we will also be interested in the absolute *number* of occurences of particular events, e.g., the number of lines in which \"fire\" occurs.   We will use $n(x)$ to represent the these numbers.\n",
        "\n",
        "Your main task for this assignment is to write Python code to analyze the PMI of tokens from Shakespeare's plays.    Based this analysis, we want to be able to answer two types of queries:\n",
        "\n",
        "* Two-Token Queries: Given a pair of tokens, $x$ and $y$, report the number of lines on which that pair co-occurs ($n(x,y)$) as well as $PMI(x,y)$.\n",
        "* One-Token Queries: Given a single token, $x$, report the number of lines on which that token occurs ($n(x)$).   In addition, report the five tokens that have the largest PMI with respect to $x$ (and their PMIs).   That is, report the five $y$'s for which $PMI(x,y)$ is largest.\n",
        "\n",
        "To avoid reporting spurious results for the one-token queries, we are only interested in token pairs that co-occur a sufficient number of times.   Therefore, we will use a *threshold* parameter for one-token queries.   A one-token query should only report pairs of tokens that co-occur at least *threshold* times in the input.   For example, given the threshold 12, a one-token query for \"fire\" the should report the five tokens that have the largest PMI (with respect to \"fire\") among all tokens that co-occur with \"fire\" on at least 12 lines.   If there are fewer than five such tokens, report fewer than five.\n",
        "\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "pBJ1H1i_ENGE"
      },
      "source": [
        "Run the next block to download the text file (`Shakespeare.txt`) and the Python tokenizer module (`simple_tokenize.py`)."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "rcXDTtuqENqF"
      },
      "source": [
        "!wget -q https://student.cs.uwaterloo.ca/~cs451/content/cs431/Shakespeare.txt\n",
        "!wget -q https://student.cs.uwaterloo.ca/~cs451/content/cs431/simple_tokenize.py"
      ],
      "execution_count": 21,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "BVFO1bonNfJo"
      },
      "source": [
        "---\n",
        "#### Question 1  (2/10 marks):\n",
        "\n",
        "Before writing code to handle the PMI queries, start writing some code to answer some simpler questions that give an\n",
        "idea of how big the PMI analysis problem will be.   The box below contains some starter code that reads in the 'Shakespeare.txt' file and tokenizes it one line at time.   (This is the same code you started with for Assignment 0.)  Extend this code to determine (a) the number of *distinct* tokens that exist in 'Shakespeare.txt', and (b) the number of \n",
        "distinct token pairs that exist in 'Shakespeare.txt'.  For the purposes of this question, consider the token pair $x,y$ to be distinct from the pair $y,x$, i.e., count them both.   Ignore token pairs of the form $x,x$."
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "collapsed": true,
        "id": "AWP7tAfsNfJp",
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "outputId": "9e3c46cf-bbc7-456c-e2f3-a372746439a4"
      },
      "source": [
        "# this imports the SimpleTokenize function from the simple_tokenize.py file that you uploaded\n",
        "from simple_tokenize import simple_tokenize\n",
        "total_line = 0\n",
        "distinct_tokens_dic = {}\n",
        "distinct_token_pairs_dic = {}\n",
        "# Now, let's tokenize Shakespeare's plays\n",
        "with open('Shakespeare.txt') as f:\n",
        "  for line in f:\n",
        "    total_line += 1\n",
        "    # tokenize, one line at a time\n",
        "    t = simple_tokenize(line)\n",
        "    distinct_token_list=[]\n",
        "    for word in t:\n",
        "      if word not in distinct_token_list:\n",
        "        distinct_token_list.append(word)\n",
        "\n",
        "    for word in distinct_token_list:\n",
        "      if word not in distinct_tokens_dic:\n",
        "        distinct_tokens_dic[word] = 0\n",
        "      distinct_tokens_dic[word] += 1\n",
        "      if word not in distinct_token_pairs_dic:\n",
        "        distinct_token_pairs_dic[word] = {}\n",
        "      for w in distinct_token_list:\n",
        "        if w != word :\n",
        "          if w not in distinct_token_pairs_dic[word]:\n",
        "            distinct_token_pairs_dic[word][w] = 0\n",
        "          distinct_token_pairs_dic[word][w] += 1\n",
        "distinct_token_pairs = 0    \n",
        "for word in distinct_token_pairs_dic:\n",
        "  distinct_token_pairs += len(distinct_token_pairs_dic[word])\n",
        "\n",
        "# The number of distinct tokens that exist in 'Shakespeare.txt'.    \n",
        "  distinct_tokens = len(distinct_tokens_dic)\n",
        "\n",
        "print(\"The number of distinct tokens that exist in 'Shakespeare.txt' is \" + str(distinct_tokens))\n",
        "print(\"The number of distinct token pairs that exist in 'Shakespeare.txt' is \" + str(distinct_token_pairs))\n",
        "          \n",
        "\n",
        "\n",
        "# extend this code to answer Question 1.\n",
        "# when your code is executed, it should print the number of distinct tokens and the number of distinct token pairs"
      ],
      "execution_count": 45,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            "The number of distinct tokens that exist in 'Shakespeare.txt' is 25975\n",
            "The number of distinct token pairs that exist in 'Shakespeare.txt' is 1969760\n",
            "122458\n"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "PpFi9CxkNfJq"
      },
      "source": [
        "---\n",
        "\n",
        "#### Question 2 (6/10 marks):\n",
        "Next, write Python code to answer the one-token and two-token queries described above, for 'Shakespeare.txt'.   The code cell below contains some starter code that implements a simple text-based query interface.  It allows a user to ask a series of one-token or two-token queries.   Try running the starter code to get a sense of how the interface behaves.    \n",
        "\n",
        "Your task is to write code to read and tokenize 'Shakespeare.txt', record information that will allow both types of PMI queries to be answered, and then answer queries that are posed through the query interface.  Make sure that your code is well commented, so that it will be clear to the markers.\n",
        "\n",
        "If you cannot get answers to both types of queries working, try to get at least one type working, for partial credit.\n"
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "3of7ltFENfJr",
        "colab": {
          "base_uri": "https://localhost:8080/",
          "height": 562
        },
        "outputId": "83a3a046-62a9-4ccc-bb9f-cc6e7e440c0b"
      },
      "source": [
        "# this imports the SimpleTokenize function from the simple_tokenize.py file that you uploaded\n",
        "from simple_tokenize import simple_tokenize\n",
        "# the log function for computing PMI\n",
        "# for the sake of consistency across solutions, please use log base 10\n",
        "from math import log\n",
        "\n",
        "###################################################################################################################\n",
        "#  replace this with your PMI analysis code, so that you can support the user interface below\n",
        "#  it should read and tokenize Shakespeare.txt, and store enough information in Python data structures\n",
        "#  to allow you to answer the PMI queries below\n",
        "###################################################################################################################\n",
        "\n",
        "###################################################################################################################\n",
        "#  the user interface below defines the types of PMI queries that users can ask\n",
        "#  you will need to modify it - where indicated - to access the results of your PMI analysis (above)\n",
        "#  so that the queries can be answered\n",
        "###################################################################################################################\n",
        "\n",
        "while True:\n",
        "    q = input(\"Input 1 or 2 space-separated tokens (return to quit): \")\n",
        "    if len(q) == 0:\n",
        "        break\n",
        "    q_tokens = simple_tokenize(q)\n",
        "    if len(q_tokens) == 1:\n",
        "        threshold = 0\n",
        "        while threshold <= 0:\n",
        "            try:\n",
        "                threshold = int(input(\"Input a positive integer frequency threshold: \"))\n",
        "            except ValueError:\n",
        "                print(\"Threshold must be a positive integer!\")\n",
        "                continue\n",
        "        \n",
        "        # Put code here to answer a One-Token Query with token q_tokens[0] and the specified threshold,\n",
        "        n_x = distinct_tokens_dic[q_tokens[0]]\n",
        "        p_x = n_x/total_line\n",
        "        top_5 = {}\n",
        "\n",
        "        for token in distinct_token_pairs_dic[q_tokens[0]]:\n",
        "          n_y = distinct_tokens_dic[token]\n",
        "          if n_y >= threshold:\n",
        "            p_y = n_y/total_line \n",
        "            n_x_y = distinct_token_pairs_dic[q_tokens[0]][token]\n",
        "            p_x_y = n_x_y/total_line\n",
        "            PMI = log((p_x_y) / (p_x * p_y), 10)\n",
        "            if len(top_5) == 0:\n",
        "              top_5[PMI] = [token]\n",
        "            else:\n",
        "              if len(top_5) < 5:\n",
        "                if PMI not in top_5:\n",
        "                  top_5[PMI] = []\n",
        "                top_5[PMI] += [token]\n",
        "              else:\n",
        "                if  PMI in top_5:\n",
        "                  top_5[PMI] += [token]\n",
        "                elif PMI > min(top_5):\n",
        "                  del top_5[min(top_5)]\n",
        "                  top_5[PMI] = [token]\n",
        "                \n",
        "        \n",
        "        # and output the result.\n",
        "        # The print() statements below exist to show you the desired output format.\n",
        "        # Replace them with your own output code, which should produce results in a similar format.\n",
        "        print(\"  n({0}) = {1}\".format(q_tokens[0], n_x))\n",
        "        print(\"  high PMI tokens with respect to {0} (threshold: {1}):\".format(q_tokens[0],threshold))\n",
        "        i = 0\n",
        "        while len(top_5) > 0 and i < 5:\n",
        "          max_pmi = max(top_5)\n",
        "          token = top_5[max_pmi][0]\n",
        "          print(\"    n({0}, {2}) = {1},  PMI({0}, {2}) = {3}\".format(q_tokens[0], distinct_tokens_dic[token], token, max_pmi))\n",
        "          if len(top_5[max_pmi]) == 1:\n",
        "            del top_5[max_pmi]\n",
        "          else:\n",
        "            del top_5[max_pmi][0]\n",
        "          i += 1\n",
        "        # in the above, all XXX values should be at least as large as the threshold\n",
        "\n",
        "    elif len(q_tokens) == 2:\n",
        "\n",
        "        # Put code here to answer a Two-Token Query with tokens q_tokens[0] and q_tokens[1]\n",
        "        n_x_y = distinct_token_pairs_dic[q_tokens[0]][q_tokens[1]]\n",
        "        n_x = distinct_tokens_dic[q_tokens[0]]\n",
        "        n_y = distinct_tokens_dic[q_tokens[1]]\n",
        "        p_x = n_x/total_line\n",
        "        p_y = n_y/total_line\n",
        "        p_x_y = n_x_y/total_line\n",
        "        PMI = log((p_x_y) / (p_x * p_y), 10)\n",
        "\n",
        "        # As was the case for the One-Token query, the print statements below show the desired output format\n",
        "        # Replace them with your own output code\n",
        "        print(\"  n({0},{1}) = {2}\".format(q_tokens[0],q_tokens[1],n_x_y))\n",
        "        print(\"  PMI({0},{1}) = {2}\".format(q_tokens[0],q_tokens[1],PMI))\n",
        "    else:\n",
        "        print(\"Input must consist of 1 or 2 space-separated tokens!\")\n"
      ],
      "execution_count": 60,
      "outputs": [
        {
          "output_type": "stream",
          "name": "stdout",
          "text": [
            "Input 1 or 2 space-separated tokens (return to quit): end\n",
            "Input a positive integer frequency threshold: 2\n",
            "  n(end) = 353\n",
            "  high PMI tokens with respect to end (threshold: 2):\n",
            "    n(end, rope's) = 5,  PMI(end, rope's) = 2.5402124568157887\n",
            "    n(end, journey's) = 2,  PMI(end, journey's) = 2.5402124568157887\n",
            "    n(end, town's) = 3,  PMI(end, town's) = 2.3641211977601073\n",
            "    n(end, altered) = 2,  PMI(end, altered) = 2.2391824611518074\n",
            "    n(end, seducing) = 2,  PMI(end, seducing) = 2.2391824611518074\n"
          ]
        },
        {
          "output_type": "error",
          "ename": "KeyboardInterrupt",
          "evalue": "ignored",
          "traceback": [
            "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
            "\u001b[0;31mKeyboardInterrupt\u001b[0m                         Traceback (most recent call last)",
            "\u001b[0;32m<ipython-input-60-ce6f2c9f5581>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m     18\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     19\u001b[0m \u001b[0;32mwhile\u001b[0m \u001b[0;32mTrue\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 20\u001b[0;31m     \u001b[0mq\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0minput\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"Input 1 or 2 space-separated tokens (return to quit): \"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     21\u001b[0m     \u001b[0;32mif\u001b[0m \u001b[0mlen\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mq\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;34m==\u001b[0m \u001b[0;36m0\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     22\u001b[0m         \u001b[0;32mbreak\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
            "\u001b[0;32m/usr/local/lib/python3.8/dist-packages/ipykernel/kernelbase.py\u001b[0m in \u001b[0;36mraw_input\u001b[0;34m(self, prompt)\u001b[0m\n\u001b[1;32m    858\u001b[0m                 \u001b[0;34m\"raw_input was called, but this frontend does not support input requests.\"\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    859\u001b[0m             )\n\u001b[0;32m--> 860\u001b[0;31m         return self._input_request(str(prompt),\n\u001b[0m\u001b[1;32m    861\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_parent_ident\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    862\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0m_parent_header\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
            "\u001b[0;32m/usr/local/lib/python3.8/dist-packages/ipykernel/kernelbase.py\u001b[0m in \u001b[0;36m_input_request\u001b[0;34m(self, prompt, ident, parent, password)\u001b[0m\n\u001b[1;32m    902\u001b[0m             \u001b[0;32mexcept\u001b[0m \u001b[0mKeyboardInterrupt\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    903\u001b[0m                 \u001b[0;31m# re-raise KeyboardInterrupt, to truncate traceback\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m--> 904\u001b[0;31m                 \u001b[0;32mraise\u001b[0m \u001b[0mKeyboardInterrupt\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"Interrupted by user\"\u001b[0m\u001b[0;34m)\u001b[0m \u001b[0;32mfrom\u001b[0m \u001b[0;32mNone\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    905\u001b[0m             \u001b[0;32mexcept\u001b[0m \u001b[0mException\u001b[0m \u001b[0;32mas\u001b[0m \u001b[0me\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    906\u001b[0m                 \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mlog\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mwarning\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"Invalid Message:\"\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mexc_info\u001b[0m\u001b[0;34m=\u001b[0m\u001b[0;32mTrue\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
            "\u001b[0;31mKeyboardInterrupt\u001b[0m: Interrupted by user"
          ]
        }
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "4zjE_iW6NfJt"
      },
      "source": [
        "---\n",
        "\n",
        "#### Question 3 (2/10 marks):\n",
        "\n",
        "Suppose that you try to run your PMI analysis on larger files:  say, 10 times, or 100 times, or 1000 times larger than 'Shakespeare.txt'.    As the input file grows larger, what will happen to the execution of your program?   Will it get slower?   How much slower?   Will it eventually fail to run?   If so, why?\n",
        "\n",
        "In the cell below, briefly (one or two paragraphs), discuss what will happen if the input to your analysis grows.  We're not looking for an exact or empirical analysis of the behaviour of your program as a function of input size.  Rather, we are looking for a discussion of trends and limits."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "4SVhvS1tNfJu"
      },
      "source": [
        "#### Answer to Question 3:\n",
        "\n",
        "My program will get slower when I run larger files with 10 times, 100 times, or 1000 times larger than 'Shakespeare.txt'. Since my code has worst-running time which is not constant or O(n). Two for statements already makes my code with running time O(n^2). Which means it will get slower with larger files.\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Vsr1IWaDNfJu"
      },
      "source": [
        "---\n",
        "Don't forget to save your workbook!   (It's a good idea to do this regularly, while you are working.)   When you are finished and you are ready to submit your assignment, download your notebook file (.ipynb) from the hub to your machine, and then follow the submission instructions in the assignment."
      ]
    }
  ]
}